package br.com.eltonpignatel.emissorNFe.enumered;

public enum StatusNfe{
	EMITIDO, REJEITADO, CANCELADO, CONTINGENCIA, DANFE, XML_INVALIDO, TRANSMITINDO, INUTILIZADO;
}
