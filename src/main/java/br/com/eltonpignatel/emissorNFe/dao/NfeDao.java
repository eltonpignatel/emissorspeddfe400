package br.com.eltonpignatel.emissorNFe.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import br.com.eltonpignatel.emissorNFe.model.Nfe;
import br.com.eltonpignatel.enumered.StatusNfe;

public class NfeDao{
	public List<Nfe> select(StatusNfe fila) {
		
		// TODO Auto-generated method stub
		List<Nfe> nfes = new ArrayList<Nfe>();
		
		nfes = BancoDeDados.listaNfes;
		
		Predicate<Nfe> filaDeNfe = Nfe -> Nfe.getFila().equals(fila.toString());

		return nfes.stream().filter(filaDeNfe).collect(Collectors.<Nfe> toList());
	}

}
