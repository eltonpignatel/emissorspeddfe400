package br.com.eltonpignatel.emissorNFe.dao;

import java.util.ArrayList;

import br.com.eltonpignatel.emissorNFe.model.Nfe;

public class BancoDeDados {
	public static ArrayList<Nfe> listaNfes = new ArrayList<Nfe>();;
	
	public BancoDeDados() {
		listaNfes.add(new Nfe(1, "NFE", 999991, 1150.50f, "Elton Pignatel Ltda.","Venda Interestadual","EMITIDO"));
		listaNfes.add(new Nfe(1, "NFE", 999992, 1250.50f, "Fulano de Tal Ltda.","Venda Interestadual","EMITIDO"));
		listaNfes.add(new Nfe(1, "NFE", 999993, 1350.50f, "Ciclano de tal Ltda.","Venda Interestadual","EMITIDO"));
		listaNfes.add(new Nfe(1, "NFE", 999994, 1450.50f, "Beltrano de tal S/A.","Venda Interestadual","EMITIDO"));
		listaNfes.add(new Nfe(1, "NFE", 999995, 1450.50f, "Beltrano de tal S/A.","Venda Interestadual","DANFE"));
		listaNfes.add(new Nfe(1, "NFE", 999996, 1450.50f, "Beltrano de tal S/A.","Venda Interestadual","XML_INVALIDO"));
		listaNfes.add(new Nfe(1, "NFE", 999997, 1450.50f, "Beltrano de tal S/A.","Venda Interestadual","XML_INVALIDO"));
	}
	
}
