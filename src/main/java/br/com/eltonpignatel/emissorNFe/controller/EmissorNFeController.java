package br.com.eltonpignatel.emissorNFe.controller;


import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import br.com.eltonpignatel.emissorNFe.ProcessaNfe;
import br.com.eltonpignatel.emissorNFe.dao.NfeDao;
import br.com.eltonpignatel.emissorNFe.model.Nfe;
import br.com.eltonpignatel.enumered.StatusNfe;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class EmissorNFeController implements Initializable {
	
	@FXML
	private Button btnDanfe;
	
	@FXML
	private TabPane tabPanePrincipal;
	
	@FXML
	private Tab tabEmitidos;
	
	@FXML
	private Tab tabXmlInvalido;
	
	@FXML
	private Tab tabRejeitados;
	
	@FXML
	private Tab tabDanfe;
	
	@FXML
	private Tab tabContingencia;
	
	@FXML
	private Tab tabCancelados;
	
	@FXML
	private Tab tabInutilizados;
	
	@FXML
	private Label lbModoTransmissao;
	
    @FXML
    private DatePicker dpEmissao;
	
    @FXML
    private TableView<Nfe> tblEmitidos;
	
    @FXML
    private TableColumn<Nfe, String> clnTab1Selecionar, clnTab1Serie, clnTab1NomeCliente, clnTab1NaturezaOperacao;
	
    @FXML
    private TableColumn<Nfe, Integer> clnTab1Documento;
	
    @FXML
    private TableColumn<Nfe, Float> clnTab1TotalNota;
    
    @FXML
    private TableView<Nfe> tblXmlInvalido;
	
    @FXML
    private TableColumn<Nfe, String> clnTab2Selecionar, clnTab2Serie, clnTab2NomeCliente, clnTab2NaturezaOperacao;
	
    @FXML
    private TableColumn<Nfe, Integer> clnTab2Documento;
	
    @FXML
    private TableColumn<Nfe, Float> clnTab2TotalNota;
	
    @FXML
    void acaoBtnDanfe(ActionEvent event) {
		System.out.println("Danfe");
		//lbModoTransmissao.setText("Modo de Transmissão: CONTINGÊNCIA");
		
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Por favor, não me clique");
        alert.show();
        
        //Código para chamar assinatura da NFE
        
        Nfe nfe = new Nfe();
        
        ProcessaNfe processaNfe = StatusNfe.ASSINAR.obterStatusNfe();
        
    }
    
    @FXML
    void acaoTabEmitidosSelectionChanged(Event event) {
    	
    	tabPanePrincipal.getTabs().forEach(tab -> {
    		if(tab.isSelected()) {
    			switch(tab.getId()) {

	    			case "tabEmitidos":
	    	    		populaEmitidos();
	    	    		break;
	    			case "tabXmlInvalido":
	    	    		populaXmlInvalido();
	    	    		break;
	    			case "tabRejeitados":	    				
	    	    		//populaRejeitados();
	    	    		break;
	    			case "tabDanfe":
	    	    		//populaDanfe();
	    	    		break;
	    			case "tabContingencia":
	    	    		//populaContingencia();
	    	    		break;
	    			case "tabCancelados":
	    	    		//populaCancelados();
	    	    		break;
	    			case "tabInutilizados":
	    	    		//populaInutilizados();
	    	    		break;
	    	    	default:
	    	    		System.out.println("Aba não encontrada-" + tab.getId());
	    	    		break;
    			}
    			
    		}
    		
    		} );
   	
    	
    }
    
    public void initialize(URL arg0, ResourceBundle arg1) {
    	dpEmissao.setValue(LocalDate.now());
    	//populaEmitidos();
    }
    
    public void populaEmitidos() {
    	populaEmitidos(new NfeDao().select(StatusNfe.EMITIDO));
    }
    
    public void populaEmitidos(List<Nfe> nfes) {
    	
    	tblEmitidos.getItems().clear();
    	
    	nfes.forEach(item-> {
			tblEmitidos.getItems().add(item);
		});
		
		clnTab1Selecionar.setCellValueFactory(null);
		clnTab1Documento.setCellValueFactory(new PropertyValueFactory<>("documento"));
		clnTab1Serie.setCellValueFactory(new PropertyValueFactory<>("serie"));
		clnTab1NomeCliente.setCellValueFactory(new PropertyValueFactory<>("nomeCliente"));
		clnTab1NaturezaOperacao.setCellValueFactory(new PropertyValueFactory<>("naturezaOperacao"));
	    clnTab1TotalNota.setCellValueFactory(new PropertyValueFactory<>("totalNota"));
    }
    
    public void populaXmlInvalido() {    
    	populaXmlInvalido(new NfeDao().select(StatusNfe.XML_INVALIDO));
    }
    
    public void populaXmlInvalido(List<Nfe> nfes){
    	
    	tblXmlInvalido.getItems().clear();
    	
    	nfes.forEach(item-> {
			tblXmlInvalido.getItems().add(item);
		});
		
		clnTab2Selecionar.setCellValueFactory(null);
		clnTab2Documento.setCellValueFactory(new PropertyValueFactory<>("documento"));
		clnTab2Serie.setCellValueFactory(new PropertyValueFactory<>("serie"));
		clnTab2NomeCliente.setCellValueFactory(new PropertyValueFactory<>("nomeCliente"));
		clnTab2NaturezaOperacao.setCellValueFactory(new PropertyValueFactory<>("naturezaOperacao"));
	    clnTab2TotalNota.setCellValueFactory(new PropertyValueFactory<>("totalNota"));
    }
    
	
}
