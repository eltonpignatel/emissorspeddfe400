package br.com.eltonpignatel.emissorNFe.model;

/**@author elton
 *
 *Representação sumarizada das tabelas SPEDNFE, FATNOTA, FATNOTAIT... e de dados adicionais como o XML
 */
public class Nfe extends Dfe{
	
    private int empresa;
	private String serie;
	private int documento;
	private float totalNota;
	private String nomeCliente;
	private String naturezaOperacao;
	private String fila;
	
	public Nfe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Nfe(int empresa, String serie, int documento, float totalNota, String nomeCliente, String naturezaOperacao, String fila) {
		super();
		this.empresa = empresa;
		this.serie = serie;
		this.documento = documento;
		this.totalNota = totalNota;
		this.nomeCliente = nomeCliente;
		this.naturezaOperacao = naturezaOperacao;
		this.fila = fila;
	}

	public int getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public int getDocumento() {
		return documento;
	}
	public void setDocumento(int documento) {
		this.documento = documento;
	}
	public float getTotalNota() {
		return totalNota;
	}
	public void setTotalNota(float totalNota) {
		this.totalNota = totalNota;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getNaturezaOperacao() {
		return naturezaOperacao;
	}

	public void setNaturezaOperacao(String naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}

	public String getFila() {
		return fila;
	}

	public void setFila(String fila) {
		this.fila = fila;
	}
	
	
}
