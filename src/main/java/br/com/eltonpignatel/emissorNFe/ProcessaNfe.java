package br.com.eltonpignatel.emissorNFe;

import br.com.eltonpignatel.emissorNFe.model.Nfe;

public interface ProcessaNfe {
	
	public void assinar(Nfe nfe);
		
}
