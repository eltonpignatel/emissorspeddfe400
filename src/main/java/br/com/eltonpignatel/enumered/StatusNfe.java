package br.com.eltonpignatel.enumered;

import br.com.eltonpignatel.emissorNFe.ProcessaAutorizacaoNfeSincrona;
import br.com.eltonpignatel.emissorNFe.ProcessaNfe;

public enum StatusNfe{
	ASSINAR {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return new ProcessaAutorizacaoNfeSincrona();
			
		}
	},
	EMITIDO {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			
		}
	}, REJEITADO {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			
		}
	}, CANCELADO {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			// TODO Auto-generated method stub
			
		}
	}, CONTINGENCIA {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			// TODO Auto-generated method stub
			
		}
	}, DANFE {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			// TODO Auto-generated method stub
			
		}
	}, XML_INVALIDO {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			// TODO Auto-generated method stub
			
		}
	}, TRANSMITINDO {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			// TODO Auto-generated method stub
			
		}
	}, INUTILIZADO {
		@Override
		public ProcessaNfe obterStatusNfe() {
			return null;
			// TODO Auto-generated method stub
			
		}
	};
	
	public abstract ProcessaNfe obterStatusNfe();
}
