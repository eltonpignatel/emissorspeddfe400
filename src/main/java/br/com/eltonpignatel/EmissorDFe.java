package br.com.eltonpignatel;

import br.com.eltonpignatel.emissorNFe.controller.EmissorNFeController;
import br.com.eltonpignatel.emissorNFe.dao.BancoDeDados;
import br.com.eltonpignatel.emissorNFe.dao.NfeDao;
import br.com.eltonpignatel.enumered.StatusNfe;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class EmissorDFe extends Application {
	static FXMLLoader loader;
	static EmissorNFeController controller;
	
    public static void main( String[] args ) {
    	BancoDeDados bd = new BancoDeDados(); 
    	processaDados();
    	launch(args);
    }
 
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			primaryStage.setTitle("Emissor de Nota Fiscal Eletrônica...");
			primaryStage.getIcons().add(new Image("images/nfe.png"));
			
			loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("view/EmissorNFe.fxml"));
			
			Scene scene = new Scene(((AnchorPane) loader.load()));
			scene.getStylesheets().add(getClass().getClassLoader().getResource("css/EmissorNFe.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void processaDados() {
		
		Thread thread = new Thread() {
			public void run(){
				try {
					Thread.sleep(3000);
					controller = (EmissorNFeController) loader.getController();
					controller.populaEmitidos(new NfeDao().select(StatusNfe.EMITIDO));
					//controller.populaXmlInvalido(new NfeDao().select(FilaNfe.XML_INVALIDO));
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Nfe Enviada");
			}
		};
		
		thread.start();
	}
	
}
